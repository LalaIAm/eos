/* ==========================================================================
   Feedback tool with screenshot attachments
   ========================================================================== */

let attachment
/* Set the required message length  */
const messageMinAllowed = 50

$(function () {
  /* Initial status feedback elements empty */
  resetFeedback()
  $('.js-message-length').text(`${messageMinAllowed} characters required`)

  /* Feedback open/close
  ========================================================================== */
  $('.js-feedback-tool-open').click(() => {
    disableTextSelection()
    $('.js-feedback-tool').toggle()
    /* Focus on the textarea once we open */
    $('.js-feedback-tool-message').focus()
  })
  $('.js-feedback-tool-close').click(() => {
    disableTextSelection()
    $('.js-feedback-tool').hide()
  })

  /* Close feedback on ESC */
  $(document).keydown((e) => {
    // if keypressed is ESC (keycode 27), hide panel-slidein
    if (e.keyCode === 27) {
      if ($('.js-feedback-tool').is(':visible')) {
        disableTextSelection()
        $('.js-feedback-tool').toggle()
      }
    }
  })

  /* Change Send button status and message length.
  ========================================================================== */
  $('.js-feedback-tool-message').keydown(function () {
    const sendBtn = $('.js-feedback-tool-send')

    /* Show message length */
    messageMinAllowed - $(this).val().length <= 0
      ? $('.js-message-length').text('')
      : $('.js-message-length').text(
          `${messageMinAllowed - $(this).val().length} characters required`
        )

    $(this).val().length >= messageMinAllowed
      ? sendBtn.removeClass('disabled')
      : sendBtn.addClass('disabled')
  })

  /* Just to toggle the screenshoot area */
  $('.js-feedback-tool-screenshoot-trigger').click(() => {
    $('.js-screenshot-name').toggle(this.checked)
  })

  /* Send form to back-end
  ========================================================================== */
  $('.js-feedback-tool-send').click(function (e) {
    if ($(this).hasClass('disabled')) {
      return false
    } else {
      validateForm()
    }
  })

  /* Finish the process once we click Finish in the last page.
  ========================================================================== */
  $('.js-feedback-tool-finish').click(() => {
    feedbackFormActions.finishAction()
  })

  /* SCREENSHOT: creates a screenshot as soon as the feedback tool is opened
  ========================================================================== */
  $('.js-screenshoot').click(() => {
    takeScreenShoot()
  })
})

/* Main controller for sendForm, showSuccess page and finishAction
========================================================================== */
const feedbackFormActions = {
  showSuccess: () => {
    $('.js-feedback-tool-card-form').hide()
    $('.js-feedback-tool-card-success').show()
  },
  finishAction: () => {
    $('.js-feedback-tool-card-success').animate(
      {
        opacity: 0.25,
        left: '+=50',
        height: 'toggle'
      },
      500,
      function () {
        // reset feedback tool so it can be re-opened
        return resetFeedback()
      }
    )
  },
  showError: () => {
    $('.js-feedback-tool-form p')
      .addClass('alert alert-danger')
      .text(
        `There was an error on our side and we couldn't send the feedback. Please contact us at eos@suse.de`
      )
  }
}

/* This method needs to be re-assigned with a sinon.fake to be unit tested */
const sendForm = (data) => {
  // eslint-disable-line prefer-const
  fetch('/api/feedback', {
    // eslint-disable-line no-undef
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((res) => {
    if (res.status === 200) {
      return feedbackFormActions.showSuccess()
    }
    if (res.status === 500) {
      return feedbackFormActions.showError()
    }
  })
}

/* This method needs to be re-assigned with a sinon.fake to be unit tested */
const resetFeedback = () => {
  // eslint-disable-line prefer-const
  $('.js-feedback-tool').hide()
  $('.js-feedback-tool-card-form').show()
  $('.js-feedback-tool-card-success').removeAttr('style').hide()
  $('.js-feedback-tool-message').val('')
  $('.js-screenshot-name').html('').hide()
  $('.js-feedback-tool-screenshoot-trigger').prop('checked', false)
}

/* Screenshoot functionality
  ========================================================================== */
const takeScreenShoot = () => {
  html2canvas(document.body, { scale: 0.3, useCORS: true, allowTaint: false }) // eslint-disable-line no-undef
    .then((canvas) => {
      /* For implementation, make sure you send this base64 as attachment */
      const base64image = canvas.toDataURL()
      attachment = base64image
      return attachment
    })
    .then(() => {
      /* Define where you want to attach the 'file' name */
      const whereToAttachName = $('.js-screenshot-name')
      return addAttachmentName(whereToAttachName)
    })
}

/* Generate attachment name based on window location and date.
========================================================================== */
const addAttachmentName = (where) => {
  const attachName = window.location.pathname.replace('/', '')

  /* Get new date to append to the window location */
  const currentDate = new Date()
  const day = currentDate.getDate()
  const month = currentDate.getMonth() + 1
  const year = currentDate.getFullYear()
  // clean it first
  where.html('')
  // append the new text
  return where.append(`${attachName}-${day}-${month}-${year}.jpg`)
}

/* Declared functions
========================================================================== */
/*
 In order to access this function from Unit Testing, we need to make it a declared function
 dont change this to arrow function or unit test will break
*/
/* Data that will be processed in the backend and sent via email */
function parseFormContent (msg) {
  const data = {
    message: msg || null,
    img: $('.js-feedback-tool-screenshoot-trigger').is(':checked')
      ? attachment
      : null,
    type: 'feedback'
  }

  return data
}

function validateForm () {
  const formMessage = $('.js-feedback-tool-message').val()
  const dataParsed = parseFormContent(formMessage)

  /* We send the form and move to next page ONLY if msg length is larger than messageMinAllowed */
  if (dataParsed.message !== null) {
    if (dataParsed.message.length > messageMinAllowed) {
      sendForm(dataParsed)
      return true
    }
  } else {
    return false
  }
}

const disableTextSelection = () => {
  $('body').toggleClass('disable-text-selection')
}
