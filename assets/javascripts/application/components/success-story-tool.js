/* ==========================================================================
   Success story tool
   ========================================================================== */
/* Set the required message length  */
const messageMinAllowed = 50

$(function () {
  /* Initial status feedback elements empty */
  resetSuccessStoryForm()
  $('.js-message-length').text(`${messageMinAllowed} characters required`)

  /* Feedback open/close
  ========================================================================== */
  $('.js-success-story-tool-open').click(() => {
    disableTextSelection()
    $('.js-success-story-tool').toggle()
    /* Focus on the textarea once we open */
    $('.js-success-story-tool-message').focus()
  })
  $('.js-success-story-tool-close').click(() => {
    disableTextSelection()
    $('.js-success-story-tool').hide()
  })

  /* Close feedback on ESC */
  $(document).keydown((e) => {
    // if keypressed is ESC (keycode 27), hide panel-slidein
    if (e.keyCode === 27) {
      if ($('.js-success-story-tool').is(':visible')) {
        disableTextSelection()
        $('.js-success-story-tool').toggle()
      }
    }
  })

  /* Change Send button status and message length.
  ========================================================================== */
  $('.js-success-story-tool-message').keydown(function () {
    const sendBtn = $('.js-success-story-tool-send')

    /* Show message length */
    messageMinAllowed - $(this).val().length <= 0
      ? $('.js-message-length').text('')
      : $('.js-message-length').text(
          `${messageMinAllowed - $(this).val().length} characters required`
        )

    $(this).val().length >= messageMinAllowed
      ? sendBtn.removeClass('disabled')
      : sendBtn.addClass('disabled')
  })

  /* Send form to back-end
  ========================================================================== */
  $('.js-success-story-tool-send').click(function (e) {
    if ($(this).hasClass('disabled')) {
      return false
    } else {
      validateSuccessStoryForm()
    }
  })

  /* Finish the process once we click Finish in the last page.
  ========================================================================== */
  $('.js-success-story-tool-finish').click(() => {
    successStoryFormActions.finishAction()
  })
})

/* Main controller for sendSuccesStoryForm, showSuccess page and finishAction
========================================================================== */
const successStoryFormActions = {
  showSuccess: () => {
    $('.js-success-story-tool-card-form').hide()
    $('.js-success-story-tool-card-success').show()
  },
  finishAction: () => {
    $('.js-success-story-tool-card-success').animate(
      {
        opacity: 0.25,
        left: '+=50',
        height: 'toggle'
      },
      500,
      function () {
        // reset feedback tool so it can be re-opened
        return resetSuccessStoryForm()
      }
    )
  },
  showError: () => {
    $('.js-success-story-tool-form p')
      .addClass('alert alert-danger')
      .text(
        `There was an error on our side and we couldn't send the form. Please contact us at eos@suse.de`
      )
  }
}

/* This method needs to be re-assigned with a sinon.fake to be unit tested */
const sendSuccesStoryForm = (data) => {
  // eslint-disable-line prefer-const
  fetch('/api/feedback', {
    // eslint-disable-line no-undef
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then((res) => {
    if (res.status === 200) {
      return successStoryFormActions.showSuccess()
    }
    if (res.status === 500) {
      return successStoryFormActions.showError()
    }
  })
}

/* This method needs to be re-assigned with a sinon.fake to be unit tested */
const resetSuccessStoryForm = () => {
  // eslint-disable-line prefer-const
  $('.js-success-story-tool').hide()
  $('.js-success-story-tool-card-form').show()
  $('.js-success-story-tool-card-success').removeAttr('style').hide()
  $('.js-success-story-tool-message').val('')
}

/* Declared functions
========================================================================== */
/*
 In order to access this function from Unit Testing, we need to make it a declared function
 dont change this to arrow function or unit test will break
*/
/* Data that will be processed in the backend and sent via email */
function parseSuccessFormContent (msg) {
  const data = {
    message: msg || null,
    type: 'story'
  }

  return data
}

function validateSuccessStoryForm () {
  const formMessage = $('.js-success-story-tool-message').val()
  const dataParsed = parseSuccessFormContent(formMessage)

  /* We send the form and move to next page ONLY if msg length is larger than messageMinAllowed */
  if (dataParsed.message !== null) {
    if (dataParsed.message.length > messageMinAllowed) {
      sendSuccesStoryForm(dataParsed)
      return true
    }
  } else {
    return false
  }
}

const disableTextSelection = () => {
  $('body').toggleClass('disable-text-selection')
}
