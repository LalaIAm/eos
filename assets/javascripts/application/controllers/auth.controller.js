$(document).ready(function () {
  $('.login-form').submit((event) => {
    event.preventDefault()

    const username = $('input[type=text]').val()
    const password = $('input[type=password]').val()

    return getToken({ username, password })
  })
})

const getToken = (params) => {
  const { username, password } = params

  try {
    return fetch('api/auth', {
      // eslint-disable-line  no-undef
      method: 'post',
      body: JSON.stringify({
        user: {
          email: username,
          password
        }
      }),
      headers: { 'Content-Type': 'application/json' }
    }).then((response) => {
      if (response.status === 200) {
        window.location.href = '/'
      }
    })
  } catch (error) {
    console.log(error)
  }
}
