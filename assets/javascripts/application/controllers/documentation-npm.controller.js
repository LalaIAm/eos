$(function () {
  renderDocumentationNpm()
})

const renderDocumentationNpm = () => {
  const target = $('.js-documentation-npm')
  /* Only call the function if the needed (right section) */
  if (target.length) {
    gitlabDocumentationNpm((result) => {
      if (result) {
        $('.js-full-page-loading').fadeOut()
        const converter = new showdown.Converter() // eslint-disable-line no-undef

        return target.append(converter.makeHtml(result))
      }
    })
  }
}
