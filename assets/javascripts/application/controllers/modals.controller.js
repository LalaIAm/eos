$(function () {
  const modal = $('.modal-custom')
  const modalCookie = Cookies.get('annoucement')
    ? JSON.parse(Cookies.get('annoucement'))
    : undefined // eslint-disable-line no-undef

  $(window).on('load', function () {
    if (modalCookie === undefined && modal.data('active')) {
      displayModal()
    } else if (
      modal.data('active') &&
      modalCookie.campaing !== modal.data('campaing')
    ) {
      displayModal()
    }

    /* Replicate close action when CTA is clicked so we hide the modal box. */
    $('.js-modal-cta').on('click', () => modal.modal('hide'))
  })

  /* Display modal and set cookies to hide it when closed. */
  function displayModal () {
    console.log('triggerd')
    modal.modal({
      show: true,
      focus: true,
      keyboard: true,
      backdrop: 'static'
    })

    modal.on('hidden.bs.modal', function (e) {
      Cookies.set(
        'annoucement',
        {
          // eslint-disable-line no-undef
          wasShown: true,
          campaing: modal.data('campaing')
        },
        { expires: 20 }
      )
    })
  }
})
