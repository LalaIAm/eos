/* Prepare local variables that will be reused in different methods */
let $successStoriesContainer, _successStoriesNextItemTemplate

$(function () {
  // Prepare success story containers
  $successStoriesContainer = $('.js-success-stories-list')
  _successStoriesNextItemTemplate = $('.js-success-stories-item').clone(true)

  $('.js-success-stories-item').remove()

  // Initiate the full collection of success stories collection
  if ($('.js-successStoriesController').length) {
    successStory()
  }
})

const successStory = () => {
  getSuccessStories((_data) => {
    // eslint-disable-line no-undef
    const successStoryData = _data.data.successes

    for (let i = 0; i < successStoryData.length; i++) {
      const newsuccessStoryItem = _successStoriesNextItemTemplate.clone(true)
      const galleryArray = [
        successStoryData[i].image_1,
        successStoryData[i].image_2,
        successStoryData[i].image_3,
        successStoryData[i].image_4
      ]
      // Add story item info
      $(newsuccessStoryItem)
        .find('.js-success-stories-title')
        .text(successStoryData[i].title)
      $(newsuccessStoryItem)
        .find('.js-success-stories-description')
        .text(successStoryData[i].description)
      $(newsuccessStoryItem)
        .find('.js-success-stories-thumb')
        .attr('src', successStoryData[i].image_thumb)
      if (
        successStoryData[i].project_link !== null &&
        successStoryData[i].project_link.length > 0
      ) {
        $(newsuccessStoryItem)
          .find('.js-success-stories-link')
          .removeClass('d-none')
          .attr('href', successStoryData[i].project_link)
      }
      // Bind the story item images to the modal
      $(newsuccessStoryItem)
        .find('.js-launch-success-modal')
        .on('click', function () {
          // Bind the title
          $('.js-success-stories-modal-title').text(successStoryData[i].title)
          const gallery = $('.js-success-stories-images')
          // Generate the image tags dynamically
          for (let i = 0; i < galleryArray.length; i++) {
            const el = $('<img>')
            el.attr('src', galleryArray[i]).addClass('modal-gallery-img')
            gallery.append(el)
          }
          resetModal()
        })
      $($successStoriesContainer).append(newsuccessStoryItem)
    }
  })
}

/* Reset the modal */
const resetModal = () => {
  // eslint-disable-line no-unused-vars
  $('.js-success-modal').on('hidden.bs.modal', () => {
    $('.js-success-stories-images').html('')
  })
}
