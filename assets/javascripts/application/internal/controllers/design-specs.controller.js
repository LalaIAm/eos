/* ==========================================================================
  Design specs
  ========================================================================== */

/* Prepare local variables that will be reused in different methods */
let $designSpecsContainer,
  _designSpecsItemTemplate,
  $designSpecsTagContainer,
  _designSpecsTagTemplate

$(function () {
  // Prepare design specs containers
  $designSpecsContainer = $('.js-design-specs-list')
  _designSpecsItemTemplate = $('.js-design-specs-item').clone(true)

  $('.js-design-specs-item').remove()

  // Initiate the full collection of design specs collection
  if ($('.js-designSpecsController').length) {
    designSpecsCollection()
  }
})

const designSpecsCollection = () => {
  getDesignSpecs((_data) => {
    /* Reverse data to show new entry first */
    const designSpecsData = _data.data.designspecs
    let designSpecsCount
    // sort by date
    const sortByDate = (a, b) => {
      return new Date(a.date).getTime() - new Date(b.date).getTime()
    }

    designSpecsData.sort(sortByDate).reverse()

    /* RegEx to get the date without hour */
    const regEx = /[0-9]*-[0-9]*-[0-9]*/gm

    for (let i = 0; i < designSpecsData.length; i++) {
      const newDesignSpecsItem = _designSpecsItemTemplate.clone(true)
      const createdDate = designSpecsData[i].date.match(regEx)

      // Add design specs info

      $(newDesignSpecsItem)
        .find('.js-design-specs-name')
        .text(designSpecsData[i].name)
      $(newDesignSpecsItem).find('.js-design-specs-date').text(createdDate)

      // Split tags
      const newtags = designSpecsData[i].tag.split(', ')

      // Prepare design specs tags containers
      $designSpecsTagContainer = $(newDesignSpecsItem).find(
        '.js-design-specs-tag-list'
      )
      _designSpecsTagTemplate = $(newDesignSpecsItem).find(
        '.js-design-specs-tag'
      )
      $(newDesignSpecsItem).find('.js-design-specs-tag').remove()

      for (let t = 0; t < newtags.length; t++) {
        const newDesignSpecsTag = _designSpecsTagTemplate.clone(true)
        if (newtags[t] === 'internal') {
          $(newDesignSpecsTag).addClass('badge-info').text(newtags[t])
        } else if (newtags[t] === 'external') {
          $(newDesignSpecsTag).addClass('badge-warning').text(newtags[t])
        } else {
          $(newDesignSpecsTag).addClass('badge-success').text(newtags[t])
        }
        $($designSpecsTagContainer).append(newDesignSpecsTag)
      }

      $(newDesignSpecsItem)
        .find('.js-design-specs-xd-specs')
        .attr('href', `${designSpecsData[i].xdSpecs}`)
      $(newDesignSpecsItem)
        .find('.js-design-specs-xd-backup')
        .attr('href', `${designSpecsData[i].xdBackup}`)
      $(newDesignSpecsItem)
        .find('.js-design-specs-trello')
        .attr('href', `${designSpecsData[i].trello}`)
      $(newDesignSpecsItem)
        .find('.js-design-specs-description')
        .text(designSpecsData[i].documentDescription)

      $($designSpecsContainer).append(newDesignSpecsItem)

      // Call countFilterLabels function in the end
      designSpecsCount = i + 1
      if (designSpecsCount === designSpecsData.length) {
        countFilterLabels() // eslint-disable-line no-undef
      }
    }
  })
}
