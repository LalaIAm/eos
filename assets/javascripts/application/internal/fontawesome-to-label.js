/* Take the class of fontawesome icons used and print them inside a label */
$(document).on('ready', () => {
  $('.fa').each(function () {
    const classes = $(this).attr('class')
    $(this).parent().find('.js-fa-class').text(classes)
  })
})
