const getMonthsAbbreviationsService = (callback) => {
  $.when(
    $.ajax({
      url: `/javascripts/application/models/months-abbreviations.json`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.log(
          `there was an error retrieving the months-abbreviations information`
        )
        callback()
      }
    })
  ).then(function (data) {
    callback(data)
  })
}
