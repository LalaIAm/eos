describe('success-story-tool', () => {
  before(() => {
    cy.server()
    cy.route('POST', '**/feedback', 'fixture:feedback-tool.json').as(
      'sendFeedback'
    )

    cy.visit('/success-stories')
    cy.get('.js-success-story-tool-open').click()
  })

  it('should not be able to submit if msg length is less than minium', () => {
    cy.get("textarea[name ='success-story-tool-message']").type('Hey')
    cy.get('.js-success-story-tool-send').should('have.class', 'disabled')
  })

  it('should be able to submit if length is longer than min required', () => {
    cy.get("textarea[name ='success-story-tool-message']").type(
      'Hey, this is a longer message so we can test the min message width'
    )
    cy.get('.js-success-story-tool-send').should('not.have.class', 'disabled')
  })
})
