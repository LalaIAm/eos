/* ==========================================================================
   Gulp - Builder manager for the Front-end
   ========================================================================== */
/* Set dependencies required for the tasks */
const gulp = require('gulp'),
  concat = require('gulp-concat'),
  filter = require('gulp-filter'),
  rename = require('gulp-rename'),
  gulpMain = require('./modules/gulp-main.js'),
  clean = require('gulp-clean'),
  minify = require('gulp-minify'),
  sass = require('gulp-sass'),
  zip = require('gulp-zip'),
  pug = require('gulp-pug'),
  replace = require('gulp-replace'),
  cleanCSS = require('gulp-clean-css'),
  { series, parallel } = require('gulp')

/* Set the folders to read and inject vendor files */
const destination = 'vendors/build/'
const origin = 'vendors/'

/* Set the filters */
const jsFilter = filter('**/*.js'),
  cssFilter = filter('**/*.css'),
  /* we need to filter out MD fonts as it will have its own filter */
  fontFilter = filter(
    [
      '**/*.{otf,eot,svg,ttf,woff,woff2}',
      '!**/MaterialIcons-Regular.{otf,eot,svg,ttf,woff,woff2}'
    ],
    { restore: true }
  )
mdIconsFilter = filter('**/MaterialIcons-Regular.{otf,eot,svg,ttf,woff,woff2}')

/* Building tasks
   ========================================================================== */

/* Clean the built folder to start a fresh building */
const cleanMain = () => {
  return gulp.src(destination, { read: false, allowEmpty: true }).pipe(clean())
}

/* Extract all css files declared in the mainfiles object */
const extractCss = () => {
  return gulp
    .src(gulpMain(origin), { allowEmpty: true })
    .pipe(cssFilter)
    .pipe(concat('vendors.css'))
    .pipe(cleanCSS())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(`${destination}css`))
}

/* Extract all js files declared in the mainfiles object */
const extractJs = () => {
  return gulp
    .src(gulpMain(origin), { allowEmpty: true })
    .pipe(jsFilter)
    .pipe(concat('vendors.js'))
    .pipe(
      minify({
        ext: {
          min: '.min.js'
        }
      })
    )
    .pipe(gulp.dest(`${destination}js`))
}

/* Extract all font files declared in the mainfiles object */
const extractFonts = () => {
  return gulp
    .src(gulpMain(origin), { allowEmpty: true })
    .pipe(fontFilter)
    .pipe(gulp.dest(destination + 'fonts')) // move all fonts, except for MD icons to the /fonts folder as per fontawesome and eos-icons default configuration
    .pipe(fontFilter.restore)
    .pipe(mdIconsFilter)
    .pipe(gulp.dest(`${destination}css`)) // Material icons .css file is configured to have the css and fonts in the same folder
}

/* Add map files to the build so that the web inspector doesn't throw and error */
const moveBootstrapMap = () => {
  return gulp
    .src('vendors/node_modules/bootstrap/dist/css/bootstrap.min.css.map')
    .pipe(gulp.dest(`${destination}css`))
}

const moveJqueryMaps = () => {
  return gulp
    .src('vendors/node_modules/jquery/dist/jquery.min.map')
    .pipe(gulp.dest(`${destination}js`))
}

/* Export all functions to be able to use them in CLI
   ========================================================================== */
// Main build [Design system]
exports.cleanMain = cleanMain
exports.extractCss = extractCss
exports.extractJs = extractJs
exports.extractFonts = extractFonts
exports.moveBootstrapMap = moveBootstrapMap
exports.moveJqueryMaps = moveJqueryMaps

/* Configure the default gulp task
   ========================================================================== */
exports.default = series(
  cleanMain,
  parallel(
    extractCss,
    extractJs,
    extractFonts,
    moveBootstrapMap,
    moveJqueryMaps
  )
)

/* ==========================================================================
  Tasks for the public layout
  ========================================================================== */
const layoutJs = () =>
  gulp
    .src([
      'assets/javascripts/application/components/menu.js',
      'assets/javascripts/application/components/submenu.js',
      'assets/javascripts/application/controllers/menu-items.controller.js',
      'assets/javascripts/application/components/tooltips.js',
      'assets/javascripts/application/components/truncate.js'
    ])
    .pipe(concat('index.js'))
    .pipe(gulp.dest('public/examples/js/'))

const layoutCompileScss = () =>
  gulp
    .src('public/examples/scss/index.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(rename('eos-style.css'))
    .pipe(gulp.dest('public/examples/css/'))

const layoutCopyScss = () =>
  gulp
    .src(
      [
        'assets/stylesheets/eos-base/**/*',
        'assets/stylesheets/eos-components/**/*',
        'assets/stylesheets/eos-elements/**/*',
        'assets/stylesheets/eos-custom-components/dropdown/**/*',
        'assets/stylesheets/eos-layout/**/*'
      ],
      { base: 'assets/stylesheets/' }
    )
    .pipe(replace('../images/sign-in-page/', '../images/'))
    .pipe(replace('images/sign-in-page/dark-theme', 'images/dark-theme'))
    .pipe(replace('images/error-page/', 'images/'))
    .pipe(gulp.dest('public/examples/scss/'))

const layoutCopyImages = () => {
  return gulp
    .src([
      'assets/images/suse-product-brand.svg',
      'assets/images/patterns/patterns-icon.svg',
      'assets/images/icons/eos-logo.svg',
      'assets/images/sign-in-page/**/*',
      'assets/images/error-page/*'
    ])
    .pipe(gulp.dest('public/examples/images'))
}

const layoutZip = () =>
  gulp
    .src(['public/examples/**/*', '!public/examples/pug/**/**'])
    .pipe(zip('examples.zip'))
    .pipe(gulp.dest('public/'))

/* Pug to HTML
========================================================================== */
/* RegEx to find and replace the feature-flag */
const cleanRegex = /data-feature-flag= docEnabled\[.*?\]/gm
/* IMPORTANT: This template literals  needs to be an exact copy of the code presented in footer-shared.pug*/
const replaceShowChangelog = `include ./changelog-content.pug`

const moveAllNeededPug = () => {
  return gulp
    .src(
      [
        './views/shared/menu/*.pug',
        './views/shared/footer/*.pug',
        './views/layouts/shared/**'
      ],
      { base: 'views/' }
    )
    .pipe(replace(cleanRegex, ' '))
    .pipe(replace(replaceShowChangelog, ''))
    .pipe(replace(`//- Demo user-profile`, ''))
    .pipe(replace('/images/patterns/', 'images/'))
    .pipe(replace('/images/icons/', 'images/'))
    .pipe(replace('/images/', 'images/'))
    .pipe(replace('extends ../layouts/sign-in', ''))
    .pipe(replace('images/sign-in-page/', 'images/'))
    .pipe(replace('images/error-page/', 'images/'))
    .pipe(gulp.dest('public/examples/pug/files_copy'))
}

const buildHTMLfromPUG = () => {
  return gulp
    .src(['public/examples/pug/*.pug', 'public/examples/pug/dark-theme/*.pug'])
    .pipe(pug({ pretty: true }))
    .pipe(gulp.dest('public/examples/'))
}

exports.generateZip = series(
  layoutCopyImages,
  layoutCopyScss,
  layoutJs,
  layoutCompileScss,
  moveAllNeededPug,
  buildHTMLfromPUG,
  layoutZip
)
