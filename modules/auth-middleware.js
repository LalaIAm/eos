const { strapiGraphql } = require('./strapi-graphql-query')

const authMiddleware = async (req, res, next) => {
  const { data } = await strapiGraphql('applications{requiresAuth}')
  const { requiresAuth } = data.applications[0]

  if (process.env.EOS_DISABLE_AUTH_LOCALLY === 'true' || !requiresAuth)
    return next()

  if (req.session.user) {
    next()
  } else {
    if (req.originalUrl !== '/auth') {
      /* We need to let express know that when the uses clicks on login, don't use the logic to check for tokens and allow the request to the api/auth */
      return req.originalUrl === '/api/auth' ? next() : res.redirect('/auth')
    } else {
      next()
    }
  }
}

module.exports = {
  authMiddleware
}
