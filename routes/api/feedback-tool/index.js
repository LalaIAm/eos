const express = require('express')
const router = express.Router()
const nodemailer = require('nodemailer')

router.post('/', (req, res, type) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.SMTP_EMAIL,
      pass: process.env.SMTP_TOKEN
    }
  })

  // TODO: when no image is attached, dont add it to the body
  const mailOptions = {
    from: 'uxuisuseteam@gmail.com',
    to: process.env.SMTP_RECIPIENT,
    subject:
      req.body.type === 'story'
        ? 'New success story message!'
        : 'New feedback message',
    html:
      req.body.type === 'story'
        ? `<p>${req.body.message}</p>`
        : `<p>${req.body.message}</p> <h5>Screenshot attached:</h5> <img src='${req.body.img}'/> `,
    attachments: req.body.type === 'story' ? null : [{ path: req.body.img }]
  }

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error)
      res.status(500).send('Something went wrong =/')
    } else {
      res.status(200).send('Message was sent!')
      console.log(`Email sent: ${info.response}`)
    }
  })
})

module.exports = router
