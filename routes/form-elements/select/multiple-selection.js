const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('form-elements/select/multiple-selection')
})

module.exports = router
