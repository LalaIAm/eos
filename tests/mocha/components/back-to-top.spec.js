describe('Back-to-top', () => {
  // create the demo element we need to test
  const bttButton = $("<button class='js-back-to-top'></button>")

  before((done) => {
    // in order to be able to test scroll, we need to change the height of the container
    $('body').css({
      height: 2400
    })
    // we need to insert the button before the tests so we can test it
    $('body').append(bttButton)
    done()
  })

  after((done) => {
    $('body').removeAttr('style')
    // this is only for the view of the tests, so it stays at top once it finished
    $(bttButton).remove()
    done()
  })

  afterEach((done) => {
    // after each test, scroll back to top
    $(document).scrollTop(0)
    done()
  })

  describe('backToTop method', () => {
    it('Should make scrollTop go back to cero', (done) => {
      // scroll to top 10px so we can test the method
      window.scroll(0, 10)
      // call the method which should take the page back up
      backToTop()
      /*
       * since backToTop() has a delay of 500,
       * we need to wait for it to see the value of scrollTop
       */
      setTimeout(() => {
        const position = $(document).scrollTop()
        expect(position).to.equal(0)
        done()
      }, 501)
    })
  })

  describe('toggleBackToTopButton method', () => {
    it('Should make the backToTop button not visible', (done) => {
      // scroll down less or equal to 801 so the button should not become visible
      window.scroll(0, 800)
      // call the method that appends the button
      toggleBackToTopButton()
      /*
       * since toggleBackToTopButton() triggers fadeOut, the default delay is 400,
       * we need to wait for it to see the value of scrollTop
       */
      setTimeout(() => {
        expect(bttButton).to.have.css('visibility', 'hidden')
        done()
      }, 501)
    })

    it('Should make the backToTop button visible', (done) => {
      // scroll down more than 800 so the button becomes visible
      window.scroll(0, 801)
      // call the method that appends the button
      toggleBackToTopButton()
      /*
       * since toggleBackToTopButton() triggers fadeOut, the default delay is 400,
       * we need to wait for it to see the value of scrollTop
       */
      setTimeout(() => {
        expect(bttButton).to.have.css('visibility', 'visible')
        done()
      }, 501)
    })
  })
})
