/* Unit test for cookies consent.
  ========================================================================== */
describe('Cookies consent', () => {
  const cookiesBanner = $("<div class='js-cookies-alert'></div>")

  before((done) => {
    $('body').append(cookiesBanner)
    done()
  })

  after((done) => {
    $(cookiesBanner).remove()
    done()
  })

  it('Should make the banner visible.', () => {
    cookieController()
    expect(cookiesBanner).to.have.css('visibility', 'visible')
  })
})
