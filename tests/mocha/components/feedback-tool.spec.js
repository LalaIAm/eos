/* Unit test for the Feedback tool
  ========================================================================== */

const showSuccess = sinon.fake()
// these functions have been declared as let in the SUT and are being overriten
// here as fakes for test purposes
resetFeedback = sinon.fake()
sendForm = sinon.fake()

describe('Feedback tool', () => {
  describe('parseFormContent', () => {
    const screenshotCheckbox = $(
      '<input class="js-feedback-tool-screenshoot-trigger" type="checkbox"></input>'
    )

    beforeEach((done) => {
      $('body').append(screenshotCheckbox)
      done()
    })

    afterEach((done) => {
      $(screenshotCheckbox).remove()
      done()
    })

    it('Should return an object with the message sent and an image', () => {
      const message = 'something'
      attachment = 'base64 image code'
      // tick the checkbox
      $(screenshotCheckbox).prop('checked', true)
      // parse content
      const response = parseFormContent(message)
      expect(response.img).to.equal(attachment)
      expect(response.message).to.equal(message)
    })

    it('Should return an object with null message and image', () => {
      // make sure the checkbox is not ticked
      $(screenshotCheckbox).prop('checked', false)
      // parse content sending no argument
      const response = parseFormContent()
      expect(response).to.have.keys(['message', 'img'])
      expect(response.img).to.be.null
      expect(response.message).to.be.null
    })
  })

  describe('feedbackFormActions', () => {
    const feedbackMessage = $(
      '<textarea class="js-feedback-tool-message"></textarea>'
    )

    beforeEach((done) => {
      $('body').append(feedbackMessage)
      done()
    })

    afterEach((done) => {
      $(feedbackMessage).remove()
      done()
    })

    it('Should reset the feedback component when finishing feedback', (done) => {
      feedbackFormActions.finishAction()
      // since there is an animation going on, we need to wait for it to finish
      setTimeout(() => {
        expect(resetFeedback).called
        done()
      }, 601)
    })

    it('Should send the form when there is enough text', (done) => {
      // Create spies
      const spy = sinon.spy(window, 'parseFormContent')

      // Insert some text into the input field
      const message =
        'This is a test message long enough to have 50 characters to send'
      $('.js-feedback-tool-message').val(message)
      // trigger the unit under test
      const validateFormResult = validateForm()
      setTimeout(() => {
        // test that it returns true, which means it called sendForm()
        expect(validateFormResult).to.be.true
        expect(sendForm).called
        // test that it called the parseFormContent
        expect(spy).calledWith(message)
        done()
      }, 601)
    })

    it('Should not send the form when there is no text', () => {
      $('.js-feedback-tool-message').val('')
      expect(validateForm()).to.be.false
    })
  })
})
