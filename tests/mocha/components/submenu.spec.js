/* ==========================================================================
Submenu component testing.
========================================================================== */
describe('Submenu', () => {
  /* Setup fake elements, before and after test configuration
    ========================================================================== */
  const navMenu = $(
    '<section class="submenu js-submenu-section"> <nav class="main-submenu visible js-submenu-visible"> <a href="#" class="submenu-item js-select-current js-feature-flag"> Item 1 </a> <a href="#" class="submenu-item js-select-current js-feature-flag"> Item 2 </a><a href="#" class="submenu-item js-select-current js-feature-flag"> Really long name for testing this functionality </a> <a href="#" class="submenu-item js-select-current js-feature-flag"> Item 4  </a> <a href="#" class="submenu-item js-select-current js-feature-flag"> Item 5 </a> <a href="#" class="submenu-item js-select-current js-feature-flag"> Item 6 </a> <a href="#" class="submenu-item js-select-current js-feature-flag"> Item 7 </a> <a href="#" class="submenu-item js-select-current js-feature-flag"> Item 8</a>  </nav> <div class="dropdown dropdown-more js-submenu-more" style="display: none;"><a class="submenu-item dropdown-toggle" data-toggle="dropdown"><i class="material-icons eos-24">#</i></a><ul class="dropdown-menu js-submenu-content dropdown-menu-left"><li><span>More</span></li><ul class="js-submenu-more-list"></ul></ul></div></section>'
  )

  /* Test dropdown
  ========================================================================== */
  describe('submenuDropdownPosition ', () => {
    beforeEach((done) => {
      const newNavMenu = navMenu.clone()
      $('body').prepend(newNavMenu)
      done()
    })

    afterEach((done) => {
      $('.js-submenu-section').remove()
      done()
    })

    it('Should be positioned on left side if screen size is smaller than 480px', (done) => {
      $('.js-submenu-section').css({ width: 470 })
      submenuDropdownPosition()
      const result = $('.js-submenu-more ul')
      expect(result).to.have.class('dropdown-menu-right')
      done()
    })

    it('Should be positioned on right side if screen size is bigger than 1365px', () => {
      $('.js-submenu-section').css({ width: 1365 })
      submenuDropdownPosition()
      const result = $('.js-submenu-more ul')
      expect(result).to.have.class('dropdown-menu-left')
    })
  })

  /* Submenu and dropdown test
  ========================================================================== */
  describe('submenuCollapse', () => {
    beforeEach((done) => {
      const newNavMenu = navMenu.clone()
      $('body').prepend(newNavMenu)
      done()
    })

    afterEach((done) => {
      $('.js-submenu-section').removeAttr('style')
      /*
        In this case we need to use the class so we create a new DOM element everytime.
        This is because we are modifying the DOM element in each test and it needs
        to come clean for the next one.
      */
      $('.js-submenu-section').remove()
      done()
    })

    it('Should move elements to the dropdown and add the hide class to them if the submenu is too small to fit all submenu items', () => {
      /*
         Since we can't use accurate size, we will use the smallest possible
         which is 320. To break this test, use a large size, like 1200
      */
      $('.js-submenu-section').css({ width: 320 })
      // we need to first call submenuLinksSetAttribute to add the sizes to all items
      // this is ust a dependency, but not part of the test itself
      submenuLinksSetAttribute()
      // Call the submenuCollapse to test the results
      submenuCollapse()
      expect($('.js-submenu-more-list a')).to.have.lengthOf.to.be.gt(1)
      expect($('.main-submenu a').last()).to.have.class('d-none')
    })

    it('Should move only 1 elements to the dropdown if the submenu is larger enough to fit all submenu items', () => {
      /*
         We use a very big size to make sure they 8 submenu links always fit.
         To break this test, use a small size, like 320
      */
      $('.js-submenu-section').css({ width: 1700 })
      // Call the submenuCollapse to test the results
      submenuCollapse()
      // we expect to have only 1, as there are 8 and the default limit is 7
      expect($('.js-submenu-more-list a')).to.have.lengthOf(1)
    })

    it('Should have the same number of elements than the variable submenuLimit', () => {
      /* We set the limit to 4 just to make sure it works with other length */
      const submenuLimit = 4
      submenuCollapse(submenuLimit)
      /* We remove the elements from the submenu */
      expect($('.main-submenu a:not(.d-none)')).to.have.lengthOf(submenuLimit)
    })
  })

  /* Test submenu class on scroll.
   ========================================================================== */
  describe('submenuAddShadow', () => {
    before((done) => {
      const newNavMenu = navMenu.clone()
      // in order to be able to test scroll, we need to change the height of the container
      $('body')
        .css({
          height: 1500
        })
        .prepend(newNavMenu)
      done()
    })

    after((done) => {
      $('body').removeAttr('style')
      $('.js-submenu-section').remove()
      done()
    })

    it('Should add the class submenu-scroll if scroll position > 100', () => {
      window.scroll(0, 101)
      submenuAddShadow()
      expect($('.submenu')).to.have.class('submenu-scroll')
    })

    it('Should remove the class submenu-scroll if scroll position <= 100', () => {
      window.scroll(0, 100)
      submenuAddShadow()
      expect($('.submenu')).to.not.have.class('submenu-scroll')
    })
  })
})
